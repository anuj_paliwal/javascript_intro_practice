function Kitten(name, age) {
	this.name = name;
	this.age = age;
}

//Why does this work?
/*
Kitten.prototype.meow_to = function (name) {
	var that = this
	var ret = function () {
		console.log(that.name + " meows to " + name);
	};

	return ret();
};

var k1 = new Kitten('bo', 12);
k1.meow_to("Brosef");
*/
//But this doesn't work 

Kitten.prototype.meow_to = function (name) {
	var that = this
	return function () {
		console.log(name);
		console.log(that.name + " meows to " + name);
	};

};

var k1 = new Kitten('bo', 12);
k1.meow_to("Brosef");





