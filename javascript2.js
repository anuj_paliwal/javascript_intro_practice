function times(num, fun) {
	for (var i = 0; i < num; i++) {
		fun();
	}
}

function Cat() {
	this.age = 5;
	//var self = this;
	this.age_one_year = function () {
		this.age += 1;
	}
};
cat = new Cat();
//cat.age_one_year();
console.log(cat.age)

times(10, cat.age_one_year.bind(cat));

console.log(cat.age)